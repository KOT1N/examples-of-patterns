package com.example;
import java.util.Scanner;

// Abstract class representing a handler in the chain
abstract class Handler {
    protected Handler successor;

    public void setSuccessor(Handler successor) {
        this.successor = successor;
    }

    public abstract void handleRequest(int request);
}

// Concrete handler class for validating client information
class ClientValidationHandler extends Handler {
    public void handleRequest(int request) {
        if (request == 1) {
            System.out.println("Client information is valid. Proceeding to the next step...");
            if (successor != null) {
                successor.handleRequest(request);
            }
        } else {
            System.out.println("Invalid client information. Registration process aborted.");
        }
    }
}

// Concrete handler class for generating client ID
class ClientIDGenerationHandler extends Handler {
    public void handleRequest(int request) {
        if (request == 2) {
            System.out.println("Client ID generated successfully. Proceeding to the next step...");
            if (successor != null) {
                successor.handleRequest(request);
            }
        } else {
            System.out.println("Client ID generation failed. Registration process aborted.");
        }
    }
}

// Concrete handler class for storing client information
class ClientStorageHandler extends Handler {
    public void handleRequest(int request) {
        if (request == 3) {
            System.out.println("Client information stored successfully. Registration process completed.");
        } else {
            System.out.println("Client information storage failed. Registration process aborted.");
        }
    }
}

// Client class that initiates the request
public class ClientRegistrationSystem {
    private Handler chain;

    public ClientRegistrationSystem() {
        // Creating the handlers
        Handler validationHandler = new ClientValidationHandler();
        Handler idGenerationHandler = new ClientIDGenerationHandler();
        Handler storageHandler = new ClientStorageHandler();

        // Setting up the chain of responsibility
        validationHandler.setSuccessor(idGenerationHandler);
        idGenerationHandler.setSuccessor(storageHandler);

        chain = validationHandler;
    }

    public void processRequest(int request) {
        chain.handleRequest(request);
    }

    public static void main(String[] args) {
        ClientRegistrationSystem system = new ClientRegistrationSystem();
        Scanner scanner = new Scanner(System.in);

        System.out.println(":: Notary Office ::");
        System.out.println("Please follow the registration process step by step.");

        // Prompting the user for each step of the registration process
        System.out.println("Step 1: Validate client information.");
        System.out.print("Enter 1 to proceed or any other key to abort: ");
        int step1 = scanner.nextInt();
        system.processRequest(step1);

        System.out.println("Step 2: Generate client ID.");
        System.out.print("Enter 2 to proceed or any other key to abort: ");
        int step2 = scanner.nextInt();
        system.processRequest(step2);

        System.out.println("Step 3: Store client information.");
        System.out.print("Enter 3 to proceed or any other key to abort: ");
        int step3 = scanner.nextInt();
        system.processRequest(step3);

        scanner.close();
    }
}
