package com.example;
public class NotaryAppointmentSystem {
    private static NotaryAppointmentSystem instance;

    private NotaryAppointmentSystem() {
        // Private constructor to prevent instantiation
    }

    public static NotaryAppointmentSystem getInstance() {
        if (instance == null) {
            synchronized (NotaryAppointmentSystem.class) {
                if (instance == null) {
                    instance = new NotaryAppointmentSystem();
                }
            }
        }
        return instance;
    }

    public void makeAppointment(String clientName, String appointmentTime) {
        System.out.println("Appointment has been made for " + clientName + " at " + appointmentTime);
        // Additional code to actually save the appointment in the system
    }

    public void cancelAppointment(String clientName, String appointmentTime) {
        System.out.println("Appointment for " + clientName + " at " + appointmentTime + " has been cancelled");
        // Additional code to remove the appointment from the system
    }
}

public class Client {
    public static void main(String[] args) {
        NotaryAppointmentSystem appointmentSystem = NotaryAppointmentSystem.getInstance();

        // Make an appointment
        appointmentSystem.makeAppointment("John", "10:00 AM");

        // Cancel an appointment
        appointmentSystem.cancelAppointment("John", "10:00 AM");
    }
}
