package com.example;
// Interface representing the states
interface AppointmentState {
    void confirmAppointment();
    void cancelAppointment();
}

// Concrete state classes
class ScheduledState implements AppointmentState {
    @Override
    public void confirmAppointment() {
        System.out.println("Appointment is already confirmed.");
    }

    @Override
    public void cancelAppointment() {
        System.out.println("Appointment canceled successfully.");
    }
}

class ConfirmedState implements AppointmentState {
    @Override
    public void confirmAppointment() {
        System.out.println("Appointment is already confirmed.");
    }

    @Override
    public void cancelAppointment() {
        System.out.println("Cannot cancel a confirmed appointment.");
    }
}

class CanceledState implements AppointmentState {
    @Override
    public void confirmAppointment() {
        System.out.println("Appointment is already canceled.");
    }

    @Override
    public void cancelAppointment() {
        System.out.println("Appointment is already canceled.");
    }
}

// Context class representing the appointment
class Appointment {
    private AppointmentState state;

    public Appointment() {
        state = new ScheduledState(); // Initial state is scheduled
    }

    public void setState(AppointmentState state) {
        this.state = state;
    }

    public void confirm() {
        state.confirmAppointment();
    }

    public void cancel() {
        state.cancelAppointment();
        if (state instanceof ScheduledState) {
            setState(new CanceledState()); // Change state to canceled
        }
    }
}

// Main class
public class ElectronicAppointmentSystem {
    public static void main(String[] args) {
        Appointment appointment = new Appointment();

        appointment.confirm(); // Output: Appointment is already confirmed.
        appointment.cancel(); // Output: Appointment canceled successfully.

        // Changing the state to Confirmed
        appointment.setState(new ConfirmedState());

        appointment.confirm(); // Output: Appointment is already confirmed.
        appointment.cancel(); // Output: Cannot cancel a confirmed appointment.
    }
}
