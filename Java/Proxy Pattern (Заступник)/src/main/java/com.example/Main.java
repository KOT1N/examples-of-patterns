package com.example;
// Subject interface
interface NotaryService {
    void registerClient(String clientName);
}

// RealSubject class
class NotaryServiceImpl implements NotaryService {
    @Override
    public void registerClient(String clientName) {
        System.out.println("Client '" + clientName + "' has been registered.");
    }
}

// Proxy class
class NotaryServiceProxy implements NotaryService {
    private NotaryService notaryService;
    private boolean isAdmin;

    public NotaryServiceProxy(String username, String password) {
        if (username.equals("admin") && password.equals("password")) {
            isAdmin = true;
            notaryService = new NotaryServiceImpl();
        } else {
            isAdmin = false;
        }
    }

    @Override
    public void registerClient(String clientName) {
        if (isAdmin) {
            notaryService.registerClient(clientName);
        } else {
            System.out.println("Only admin users can register clients.");
        }
    }
}

// Client class
public class Main {
    public static void main(String[] args) {
        // Admin user
        NotaryService adminService = new NotaryServiceProxy("admin", "password");
        adminService.registerClient("John Doe");

        // Non-admin user
        NotaryService regularService = new NotaryServiceProxy("user", "pass123");
        regularService.registerClient("Jane Smith");
    }
}
