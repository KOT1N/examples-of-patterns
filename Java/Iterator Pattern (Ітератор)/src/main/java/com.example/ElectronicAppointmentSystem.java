package com.example;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Define the Client class
class Client {
  private String name;
  private String appointmentDate;

  public Client(String name, String appointmentDate) {
    this.name = name;
    this.appointmentDate = appointmentDate;
  }

  public String getName() {
    return name;
  }

  public String getAppointmentDate() {
    return appointmentDate;
  }
}

// Define the AppointmentList class implementing the Iterator pattern
class AppointmentList implements Iterable<Client> {
  private List<Client> clients;

  public AppointmentList() {
    clients = new ArrayList<>();
  }

  public void addClient(Client client) {
    clients.add(client);
  }

  @Override
  public Iterator<Client> iterator() {
    return new AppointmentIterator();
  }

  // Define the AppointmentIterator class
  private class AppointmentIterator implements Iterator<Client> {
    private int position;

    public AppointmentIterator() {
      position = 0;
    }

    @Override
    public boolean hasNext() {
      return position < clients.size();
    }

    @Override
    public Client next() {
      if (this.hasNext()) {
        return clients.get(position++);
      }
      return null;
    }
  }
}

// Main class to test the implementation
public class ElectronicAppointmentSystem {
  public static void main(String[] args) {
    AppointmentList appointmentList = new AppointmentList();

    // Add sample clients
    appointmentList.addClient(new Client("John", "2022-01-10"));
    appointmentList.addClient(new Client("Alice", "2022-01-15"));
    appointmentList.addClient(new Client("Bob", "2022-01-20"));

    // Iterate over the clients using Iterator pattern
    Iterator<Client> iterator = appointmentList.iterator();
    while (iterator.hasNext()) {
      Client client = iterator.next();
      System.out.println("Client: " + client.getName() + ", Appointment Date: " + client.getAppointmentDate());
    }
  }
}
