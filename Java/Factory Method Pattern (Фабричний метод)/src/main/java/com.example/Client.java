package com.example;
// Interface for the product
interface ElectronicRecord {
    void createRecord();
}

// Concrete implementation of ElectronicRecord
class ClientRecord implements ElectronicRecord {
    @Override
    public void createRecord() {
        System.out.println("Creating client record...");
        // Code to create client record goes here
    }
}

// Concrete implementation of ElectronicRecord
class DocumentRecord implements ElectronicRecord {
    @Override
    public void createRecord() {
        System.out.println("Creating document record...");
        // Code to create document record goes here
    }
}

// Factory class to create the specific ElectronicRecord objects
class RecordFactory {
    public static ElectronicRecord createRecord(String recordType) {
        if (recordType.equalsIgnoreCase("client")) {
            return new ClientRecord();
        } else if (recordType.equalsIgnoreCase("document")) {
            return new DocumentRecord();
        } else {
            throw new IllegalArgumentException("Invalid record type.");
        }
    }
}

// Main class to test the factory method pattern
public class Client {
    public static void main(String[] args) {
        // Create a client record
        ElectronicRecord clientRecord = RecordFactory.createRecord("client");
        clientRecord.createRecord();

        // Create a document record
        ElectronicRecord documentRecord = RecordFactory.createRecord("document");
        documentRecord.createRecord();
    }
}
