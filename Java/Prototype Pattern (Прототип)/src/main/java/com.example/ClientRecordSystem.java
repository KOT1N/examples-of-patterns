package com.example;
import java.util.HashMap;
import java.util.Map;

// Prototype interface
interface ClientRecord {
    ClientRecord clone();
    void printDetails();
}

// Concrete prototype 1
class IndividualClientRecord implements ClientRecord {
    private String name;
    private String address;
    private String contact;

    public IndividualClientRecord(String name, String address, String contact) {
        this.name = name;
        this.address = address;
        this.contact = contact;
    }

    public ClientRecord clone() {
        return new IndividualClientRecord(name, address, contact);
    }

    public void printDetails() {
        System.out.println("Individual Client Record:");
        System.out.println("Name: " + name);
        System.out.println("Address: " + address);
        System.out.println("Contact: " + contact);
    }
}

// Concrete prototype 2
class BusinessClientRecord implements ClientRecord {
    private String companyName;
    private String companyAddress;
    private String contactPerson;

    public BusinessClientRecord(String companyName, String companyAddress, String contactPerson) {
        this.companyName = companyName;
        this.companyAddress = companyAddress;
        this.contactPerson = contactPerson;
    }

    public ClientRecord clone() {
        return new BusinessClientRecord(companyName, companyAddress, contactPerson);
    }

    public void printDetails() {
        System.out.println("Business Client Record:");
        System.out.println("Company Name: " + companyName);
        System.out.println("Company Address: " + companyAddress);
        System.out.println("Contact Person: " + contactPerson);
    }
}

// Client class
public class ClientRecordSystem {
    public static void main(String[] args) {
        // Creating initial client records
        ClientRecord initialIndividualRecord = new IndividualClientRecord("John Doe", "123 Main St", "john.doe@example.com");
        ClientRecord initialBusinessRecord = new BusinessClientRecord("ABC Corp", "456 Business Ave", "Jane Smith");

        // Creating a record cache
        Map<String, ClientRecord> recordCache = new HashMap<String, ClientRecord>();
        recordCache.put("individual", initialIndividualRecord);
        recordCache.put("business", initialBusinessRecord);

        // Cloning and updating individual client record
        ClientRecord clonedIndividualRecord = recordCache.get("individual").clone();
        ((IndividualClientRecord) clonedIndividualRecord).setContact("jdoe@gmail.com");

        // Cloning and updating business client record
        ClientRecord clonedBusinessRecord = recordCache.get("business").clone();
        ((BusinessClientRecord) clonedBusinessRecord).setContactPerson("Bob Johnson");

        // Adding the updated records to the cache
        recordCache.put("individual", clonedIndividualRecord);
        recordCache.put("business", clonedBusinessRecord);

        // Printing all client records
        for (ClientRecord record : recordCache.values()) {
            record.printDetails();
            System.out.println();
        }
    }
}
