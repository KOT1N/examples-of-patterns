package com.example;
import java.util.ArrayList;
import java.util.List;

// Abstract class representing the notary service
abstract class NotaryService {
    protected String serviceName;

    public abstract void bookAppointment(String clientName);
}

// Concrete implementation of NotaryService providing service for notarial acts
class NotarialActsService extends NotaryService {
    public NotarialActsService() {
        serviceName = "Notarial Acts";
    }

    @Override
    public void bookAppointment(String clientName) {
        System.out.println("Booking an appointment for " + serviceName + " with " + clientName);
        // Additional logic specific to booking notarial acts appointment
    }
}

// Concrete implementation of NotaryService providing service for document certification
class CertificationService extends NotaryService {
    public CertificationService() {
        serviceName = "Certification";
    }

    @Override
    public void bookAppointment(String clientName) {
        System.out.println("Booking an appointment for " + serviceName + " with " + clientName);
        // Additional logic specific to booking certification appointment
    }
}

// Context class that allows clients to book appointments through different services
class ClientBookingSystem {
    private NotaryService service;

    public ClientBookingSystem(NotaryService service) {
        this.service = service;
    }

    public void setService(NotaryService service) {
        this.service = service;
    }

    public void bookAppointment(String clientName) {
        service.bookAppointment(clientName);
    }
}

// Client class that demonstrates the usage of the program
public class Main {
    public static void main(String[] args) {
        // Create instances of concrete services
        NotarialActsService notarialActsService = new NotarialActsService();
        CertificationService certificationService = new CertificationService();

        // Create the client booking system and book appointments through different services
        ClientBookingSystem bookingSystem = new ClientBookingSystem(notarialActsService);
        bookingSystem.bookAppointment("John Smith");

        bookingSystem.setService(certificationService);
        bookingSystem.bookAppointment("Alice Johnson");
    }
}
