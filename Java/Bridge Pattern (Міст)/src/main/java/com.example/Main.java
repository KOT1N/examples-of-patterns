package com.example;
// Abstraction interface
interface Client {
    void registerAppointment();
}

// Refined abstraction classes
class IndividualClient implements Client {
    private final AppointmentScheduler appointmentScheduler;

    public IndividualClient(AppointmentScheduler appointmentScheduler) {
        this.appointmentScheduler = appointmentScheduler;
    }

    @Override
    public void registerAppointment() {
        System.out.println("Registering an appointment for an individual client.");
        appointmentScheduler.scheduleAppointment();
    }
}

class CorporateClient implements Client {
    private final AppointmentScheduler appointmentScheduler;

    public CorporateClient(AppointmentScheduler appointmentScheduler) {
        this.appointmentScheduler = appointmentScheduler;
    }

    @Override
    public void registerAppointment() {
        System.out.println("Registering an appointment for a corporate client.");
        appointmentScheduler.scheduleAppointment();
    }
}

// Implementor interface
interface AppointmentScheduler {
    void scheduleAppointment();
}

// Concrete implementor classes
class OnlineAppointmentScheduler implements AppointmentScheduler {
    @Override
    public void scheduleAppointment() {
        System.out.println("Appointment scheduled online.");
    }
}

class OfflineAppointmentScheduler implements AppointmentScheduler {
    @Override
    public void scheduleAppointment() {
        System.out.println("Appointment scheduled offline.");
    }
}

// Client code
public class Main {
    public static void main(String[] args) {
        AppointmentScheduler onlineScheduler = new OnlineAppointmentScheduler();
        AppointmentScheduler offlineScheduler = new OfflineAppointmentScheduler();

        Client individualClient = new IndividualClient(onlineScheduler);
        individualClient.registerAppointment();

        Client corporateClient = new CorporateClient(offlineScheduler);
        corporateClient.registerAppointment();
    }
}
