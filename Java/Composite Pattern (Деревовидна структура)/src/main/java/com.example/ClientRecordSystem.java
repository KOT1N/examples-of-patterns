package com.example;
import java.util.ArrayList;
import java.util.List;

// Component
interface ClientRecord {
    void print();
}

// Leaf
class LeafClientRecord implements ClientRecord {
    private String name;

    public LeafClientRecord(String name) {
        this.name = name;
    }

    public void print() {
        System.out.println("Client Record: " + name);
    }
}

// Composite
class CompositeClientRecord implements ClientRecord {
    private String name;
    private List<ClientRecord> children;

    public CompositeClientRecord(String name) {
        this.name = name;
        this.children = new ArrayList<>();
    }

    public void add(ClientRecord clientRecord) {
        children.add(clientRecord);
    }

    public void remove(ClientRecord clientRecord) {
        children.remove(clientRecord);
    }

    public void print() {
        System.out.println("Client Record: " + name);
        for (ClientRecord clientRecord : children) {
            clientRecord.print();
        }
    }
}

public class ClientRecordSystem {
    public static void main(String[] args) {
        // Creating client records
        ClientRecord client1 = new LeafClientRecord("John Doe");
        ClientRecord client2 = new LeafClientRecord("Jane Smith");
        ClientRecord client3 = new LeafClientRecord("Robert Johnson");

        // Creating composite client record
        CompositeClientRecord composite1 = new CompositeClientRecord("Company 1");
        composite1.add(client1);
        composite1.add(client2);

        CompositeClientRecord composite2 = new CompositeClientRecord("Company 2");
        composite2.add(client3);

        CompositeClientRecord composite3 = new CompositeClientRecord("Main Company");
        composite3.add(composite1);
        composite3.add(composite2);

        // Printing client records
        composite3.print();
    }
}
