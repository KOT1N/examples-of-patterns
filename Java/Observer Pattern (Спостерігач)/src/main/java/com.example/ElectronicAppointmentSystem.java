package com.example;
import java.util.ArrayList;
import java.util.List;

// Subject interface
interface AppointmentScheduler {
    void addObserver(Observer observer);
    void removeObserver(Observer observer);
    void notifyObservers();
}

// Concrete subject
class NotaryOffice implements AppointmentScheduler {
    private List<Observer> observers = new ArrayList<>();
    private String appointmentDate;

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
        notifyObservers();
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }
}

// Observer interface
interface Observer {
    void update();
}

// Concrete observer 1
class Client implements Observer {
    private String name;
    private NotaryOffice office;

    public Client(String name, NotaryOffice office) {
        this.name = name;
        this.office = office;
        office.addObserver(this);
    }

    public void cancelAppointment() {
        office.removeObserver(this);
    }

    @Override
    public void update() {
        System.out.println(name + ": New appointment date is set for " + office.getAppointmentDate());
    }
}

// Concrete observer 2
class Manager implements Observer {
    private String name;
    private NotaryOffice office;

    public Manager(String name, NotaryOffice office) {
        this.name = name;
        this.office = office;
        office.addObserver(this);
    }

    @Override
    public void update() {
        System.out.println(name + ": New appointment date is set for " + office.getAppointmentDate());
    }
}

// Main class
public class ElectronicAppointmentSystem {
    public static void main(String[] args) {
        NotaryOffice office = new NotaryOffice();

        // Create clients
        Client client1 = new Client("Client 1", office);
        Client client2 = new Client("Client 2", office);

        // Create manager
        Manager manager = new Manager("Manager", office);

        // Set appointment date
        office.setAppointmentDate("2022-10-31");

        // Client 2 cancels appointment
        client2.cancelAppointment();

        // Set new appointment date
        office.setAppointmentDate("2022-11-15");
    }
}
