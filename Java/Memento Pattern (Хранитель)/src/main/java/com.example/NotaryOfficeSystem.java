package com.example;
import java.util.ArrayList;
import java.util.List;

// Originator class
class Client {
    private String name;
    private String phoneNumber;

    public void setName(String name) {
        this.name = name;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Memento save() {
        return new Memento(name, phoneNumber);
    }

    public void restore(Memento memento) {
        this.name = memento.getName();
        this.phoneNumber = memento.getPhoneNumber();
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}

// Memento class
class Memento {
    private final String name;
    private final String phoneNumber;

    public Memento(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}

// Caretaker class
class Caretaker {
    private final List<Memento> mementos = new ArrayList<>();

    public void addMemento(Memento memento) {
        mementos.add(memento);
    }

    public Memento getMemento(int index) {
        return mementos.get(index);
    }
}

// Client code to test the Memento pattern
public class NotaryOfficeSystem {
    public static void main(String[] args) {
        // Create a Notary Office System
        Caretaker caretaker = new Caretaker();
        Client client = new Client();

        // Make changes to the client object
        client.setName("John Doe");
        client.setPhoneNumber("+1234567890");

        // Save the current state of the client
        caretaker.addMemento(client.save());

        // Make more changes to the client object
        client.setName("Jane Smith");
        client.setPhoneNumber("+9876543210");

        // Save the new state of the client
        caretaker.addMemento(client.save());

        // Restore the previous state of the client
        client.restore(caretaker.getMemento(0));

        // Print the current state of the client
        System.out.println("Current state of the client: " + client);
    }
}
