package com.example;
import java.util.ArrayList;
import java.util.List;

// Client class representing a client's information
class Client {
    private String name;
    private String phone;
    private String email;

    public Client(String name, String phone, String email) {
        this.name = name;
        this.phone = phone;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }
}

// Object pool class for managing reusable client objects
class ClientPool {
    private static final int MAX_POOL_SIZE = 5;
    private List<Client> availableClients;

    public ClientPool() {
        availableClients = new ArrayList<>();

        for (int i = 0; i < MAX_POOL_SIZE; i++) {
            availableClients.add(createClient());
        }
    }

    public Client getClient() {
        if (availableClients.isEmpty()) {
            System.out.println("All clients are currently in use. Please try again later.");
            return null;
        }

        Client client = availableClients.remove(0);
        System.out.println("Client " + client.getName() + " is assigned.");

        return client;
    }

    public void releaseClient(Client client) {
        if (!availableClients.contains(client) && availableClients.size() < MAX_POOL_SIZE) {
            availableClients.add(client);
            System.out.println("Client " + client.getName() + " is released.");
        }
    }

    private Client createClient() {
        // Simulating creation of a client object
        return new Client("John Doe", "1234567890", "john.doe@example.com");
    }
}

// Main class to demonstrate the program
public class ObjectPoolExample {
    public static void main(String[] args) {
        // Create an object pool
        ClientPool pool = new ClientPool();

        // Acquire clients from the object pool
        Client client1 = pool.getClient();
        Client client2 = pool.getClient();

        // Release clients back to the object pool
        pool.releaseClient(client1);
        pool.releaseClient(client2);

        // Attempt to acquire more clients than the pool can handle
        Client client3 = pool.getClient();
        Client client4 = pool.getClient();
        Client client5 = pool.getClient();
        Client client6 = pool.getClient();

        // Release one client to accommodate another
        pool.releaseClient(client3);
        Client client7 = pool.getClient();

        // Release all the clients
        pool.releaseClient(client4);
        pool.releaseClient(client5);
        pool.releaseClient(client6);
        pool.releaseClient(client7);
    }
}
