package com.example;
import java.util.ArrayList;
import java.util.List;

// Visitor interface
interface ClientVisitor {
    void visit(RegularClient client);
    void visit(PrimeClient client);
}

// Concrete Visitor
class NotaryOfficeVisitor implements ClientVisitor {
    @Override
    public void visit(RegularClient client) {
        System.out.println("Regular client: " + client.getName() + ", has requested an appointment.");
    }

    @Override
    public void visit(PrimeClient client) {
        System.out.println("Prime client: " + client.getName() + ", has requested an appointment.");
    }
}

// Element interface
interface Client {
    void accept(ClientVisitor visitor);
}

// Concrete Elements
class RegularClient implements Client {
    private String name;

    public RegularClient(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void accept(ClientVisitor visitor) {
        visitor.visit(this);
    }
}

class PrimeClient implements Client {
    private String name;

    public PrimeClient(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void accept(ClientVisitor visitor) {
        visitor.visit(this);
    }
}

// Client code
public class NotaryOfficeApp {
    public static void main(String[] args) {
        List<Client> clients = new ArrayList<>();
        clients.add(new RegularClient("John Doe"));
        clients.add(new PrimeClient("Jane Smith"));

        ClientVisitor visitor = new NotaryOfficeVisitor();

        for (Client client : clients) {
            client.accept(visitor);
        }
    }
}
