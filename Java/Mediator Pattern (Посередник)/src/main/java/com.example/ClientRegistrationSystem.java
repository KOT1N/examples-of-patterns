package com.example;
import java.util.ArrayList;
import java.util.List;

// Mediator interface
interface Mediator {
    void sendMessage(String message, Colleague colleague);
}

// Colleague interface
abstract class Colleague {
    protected Mediator mediator;

    public Colleague(Mediator mediator) {
        this.mediator = mediator;
    }

    public abstract void receiveMessage(String message);
    public abstract void sendMessage(String message);
}

// Concrete mediator
class NotaryOffice implements Mediator {
    private List<Colleague> colleagues;

    public NotaryOffice() {
        this.colleagues = new ArrayList<>();
    }

    public void addColleague(Colleague colleague) {
        colleagues.add(colleague);
    }

    @Override
    public void sendMessage(String message, Colleague colleague) {
        for (Colleague c : colleagues) {
            if (c != colleague) {
                c.receiveMessage(message);
            }
        }
    }
}

// Concrete colleague
class Client extends Colleague {
    private String name;

    public Client(String name, Mediator mediator) {
        super(mediator);
        this.name = name;
    }

    @Override
    public void receiveMessage(String message) {
        System.out.println(name + " received message: " + message);
    }

    @Override
    public void sendMessage(String message) {
        mediator.sendMessage(message, this);
    }
}

// Main class
public class ClientRegistrationSystem {
    public static void main(String[] args) {
        NotaryOffice office = new NotaryOffice();

        Client client1 = new Client("John", office);
        Client client2 = new Client("Alice", office);
        Client client3 = new Client("Bob", office);

        office.addColleague(client1);
        office.addColleague(client2);
        office.addColleague(client3);

        client1.sendMessage("Hello everyone!");
    }
}
