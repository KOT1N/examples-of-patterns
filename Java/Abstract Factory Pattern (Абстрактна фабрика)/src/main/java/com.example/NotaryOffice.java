package com.example;
// Abstract Factory Interface
interface NotaryBookingFactory {
    Booking createBooking();
    Client createClient();
}

// Concrete Factory 1
class MeetingBookingFactory implements NotaryBookingFactory {
    public Booking createBooking() {
        return new MeetingBooking();
    }
    public Client createClient() {
        return new IndividualClient();
    }
}

// Concrete Factory 2
class OnlineBookingFactory implements NotaryBookingFactory {
    public Booking createBooking() {
        return new OnlineBooking();
    }
    public Client createClient() {
        return new CorporateClient();
    }
}

// Abstract Product 1
interface Booking {
    void scheduleBooking();
}

// Concrete Product 1
class MeetingBooking implements Booking {
    public void scheduleBooking() {
        System.out.println("Meeting booking scheduled.");
    }
}

// Concrete Product 2
class OnlineBooking implements Booking {
    public void scheduleBooking() {
        System.out.println("Online booking scheduled.");
    }
}

// Abstract Product 2
interface Client {
    void getClientDetails();
}

// Concrete Product 2
class IndividualClient implements Client {
    public void getClientDetails() {
        System.out.println("Individual client details retrieved.");
    }
}

// Concrete Product 2
class CorporateClient implements Client {
    public void getClientDetails() {
        System.out.println("Corporate client details retrieved.");
    }
}

// Client using Abstract Factory
public class NotaryOffice {
    public static void main(String[] args) {
        NotaryBookingFactory meetingFactory = new MeetingBookingFactory();
        Booking meetingBooking = meetingFactory.createBooking();
        meetingBooking.scheduleBooking();
        Client individualClient = meetingFactory.createClient();
        individualClient.getClientDetails();

        NotaryBookingFactory onlineFactory = new OnlineBookingFactory();
        Booking onlineBooking = onlineFactory.createBooking();
        onlineBooking.scheduleBooking();
        Client corporateClient = onlineFactory.createClient();
        corporateClient.getClientDetails();
    }
}
