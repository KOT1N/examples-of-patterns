package com.example;
import java.util.HashMap;
import java.util.Map;

// Flyweight interface
interface Customer {
    void recordDetails(String details);
}

// Concrete flyweight class
class CustomerRecord implements Customer {
    private String name;
    private String address;

    public CustomerRecord(String name) {
        this.name = name;
    }

    @Override
    public void recordDetails(String address) {
        this.address = address;
        System.out.println("Customer Name: " + name + ", Address: " + address);
    }
}

// Flyweight factory class
class CustomerRecordFactory {
    private Map<String, Customer> recordMap;

    public CustomerRecordFactory() {
        recordMap = new HashMap<>();
    }

    public Customer getCustomerRecord(String name) {
        Customer record = recordMap.get(name);

        if (record == null) {
            record = new CustomerRecord(name);
            recordMap.put(name, record);
        }

        return record;
    }
}

// Client class
class NotaryOffice {
    private CustomerRecordFactory recordFactory;

    public NotaryOffice(CustomerRecordFactory recordFactory) {
        this.recordFactory = recordFactory;
    }

    public void createCustomer(String name, String address) {
        Customer customer = recordFactory.getCustomerRecord(name);
        customer.recordDetails(address);
    }
}

// Main class
public class NotaryOfficeSystem {
    public static void main(String[] args) {
        CustomerRecordFactory recordFactory = new CustomerRecordFactory();
        NotaryOffice notaryOffice = new NotaryOffice(recordFactory);

        // Creating and recording customer details
        notaryOffice.createCustomer("John Doe", "123 Main St");
        notaryOffice.createCustomer("Jane Smith", "456 Oak St");
        notaryOffice.createCustomer("John Doe", "789 Elm St");
    }
}
