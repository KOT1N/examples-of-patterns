package com.example;
import java.util.ArrayList;
import java.util.List;

// Abstract Command
interface Command {
    void execute();
}

// Receiver
class NotaryOffice {
    void bookAppointment(String clientName) {
        System.out.println("Booking an appointment for client: " + clientName);
    }
}

// Concrete Command
class BookAppointmentCommand implements Command {
    private final NotaryOffice notaryOffice;
    private final String clientName;

    BookAppointmentCommand(NotaryOffice notaryOffice, String clientName) {
        this.notaryOffice = notaryOffice;
        this.clientName = clientName;
    }

    public void execute() {
        notaryOffice.bookAppointment(clientName);
    }
}

// Invoker
class CustomerBookingSystem {
    private final List<Command> commands = new ArrayList<>();

    void addCommand(Command command) {
        commands.add(command);
    }

    void executeCommands() {
        for (Command command : commands) {
            command.execute();
        }
        commands.clear();
    }
}

// Client
public class Main {
    public static void main(String[] args) {
        NotaryOffice notaryOffice = new NotaryOffice();
        CustomerBookingSystem bookingSystem = new CustomerBookingSystem();

        // Creating and executing command to book an appointment
        Command bookAppointmentCmd = new BookAppointmentCommand(notaryOffice, "John Doe");
        bookingSystem.addCommand(bookAppointmentCmd);
        bookingSystem.executeCommands();

        // Creating and executing another command to book another appointment
        Command bookAppointmentCmd2 = new BookAppointmentCommand(notaryOffice, "Jane Smith");
        bookingSystem.addCommand(bookAppointmentCmd2);
        bookingSystem.executeCommands();
    }
}
