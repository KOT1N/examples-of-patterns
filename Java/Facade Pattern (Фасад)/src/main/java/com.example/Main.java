package com.example;
// Step 1: Create individual components/classes for different functionalities

// Customer class to represent customer details
class Customer {
    private String name;

    public Customer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

// BookingSystem class to handle booking-related operations
class BookingSystem {
    public void bookAppointment(Customer customer) {
        System.out.println("Appointment booked for customer: " + customer.getName());
    }
}

// PaymentSystem class to handle payment-related operations
class PaymentSystem {
    public void processPayment(Customer customer, double amount) {
        System.out.println("Payment processed for customer: " + customer.getName() + ", Amount: " + amount);
    }
}

// Step 2: Create a Facade class that simplifies the complex interactions

class BookingFacade {
    private BookingSystem bookingSystem;
    private PaymentSystem paymentSystem;

    public BookingFacade() {
        bookingSystem = new BookingSystem();
        paymentSystem = new PaymentSystem();
    }

    public void bookAndPay(Customer customer, double amount) {
        bookingSystem.bookAppointment(customer);
        paymentSystem.processPayment(customer, amount);
    }
}

// Step 3: Test the Facade Pattern implementation

public class Main {
    public static void main(String[] args) {
        // Create a customer
        Customer customer = new Customer("John");

        // Create a facade object
        BookingFacade bookingFacade = new BookingFacade();

        // Use the facade to book an appointment and process payment
        bookingFacade.bookAndPay(customer, 100.0);
    }
}
