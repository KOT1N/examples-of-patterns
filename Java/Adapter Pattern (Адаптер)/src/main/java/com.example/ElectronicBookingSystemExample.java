package com.example;
// Target interface
interface ElectronicBookingSystem {
    void bookAppointment(String clientName, String appointmentDate);
}

// Adaptee class
class NotaryOffice {
    public void bookNotary(String notaryName, String appointmentDate) {
        System.out.println("Booking notary " + notaryName + " for appointment on " + appointmentDate);
    }
}

// Adapter class
class NotaryOfficeAdapter implements ElectronicBookingSystem {
    private NotaryOffice notaryOffice;

    public NotaryOfficeAdapter(NotaryOffice notaryOffice) {
        this.notaryOffice = notaryOffice;
    }

    public void bookAppointment(String clientName, String appointmentDate) {
        // Convert the client booking to the format of the NotaryOffice class
        String[] splitName = clientName.split(" ");
        String notaryName = splitName[0] + " " + splitName[1].charAt(0) + "."; // "John Smith" -> "John S."

        // Call the NotaryOffice's method to book the appointment
        notaryOffice.bookNotary(notaryName, appointmentDate);
    }
}

// Client class
class Client {
    private ElectronicBookingSystem bookingSystem;

    public Client(ElectronicBookingSystem bookingSystem) {
        this.bookingSystem = bookingSystem;
    }

    public void bookAppointment(String clientName, String appointmentDate) {
        bookingSystem.bookAppointment(clientName, appointmentDate);
    }
}

// Main class
public class ElectronicBookingSystemExample {
    public static void main(String[] args) {
        // Create an instance of the Adaptee class (NotaryOffice)
        NotaryOffice notaryOffice = new NotaryOffice();

        // Create an instance of the Adapter class, passing the Adaptee to it
        ElectronicBookingSystem bookingSystem = new NotaryOfficeAdapter(notaryOffice);

        // Create a Client and make a booking through the Adapter
        Client client = new Client(bookingSystem);
        client.bookAppointment("John Smith", "2022-01-01");
    }
}
