package com.example;
interface Booking {
    void book();
}

class NotaryOffice implements Booking {
    @Override
    public void book() {
        System.out.println("Booking system for the notary office");
    }
}

class BookingDecorator implements Booking {
    protected Booking decoratedBooking;

    public BookingDecorator(Booking decoratedBooking) {
        this.decoratedBooking = decoratedBooking;
    }

    @Override
    public void book() {
        decoratedBooking.book();
    }
}

class ClientBookingDecorator extends BookingDecorator {
    public ClientBookingDecorator(Booking decoratedBooking) {
        super(decoratedBooking);
    }

    @Override
    public void book() {
        super.book();
        addClientBookingFunctionality();
    }

    private void addClientBookingFunctionality() {
        System.out.println("Adding client booking functionality");
    }
}

class ScheduleBookingDecorator extends BookingDecorator {
    public ScheduleBookingDecorator(Booking decoratedBooking) {
        super(decoratedBooking);
    }

    @Override
    public void book() {
        super.book();
        addScheduleBookingFunctionality();
    }

    private void addScheduleBookingFunctionality() {
        System.out.println("Adding schedule booking functionality");
    }
}

class NotificationBookingDecorator extends BookingDecorator {
    public NotificationBookingDecorator(Booking decoratedBooking) {
        super(decoratedBooking);
    }

    @Override
    public void book() {
        super.book();
        addNotificationBookingFunctionality();
    }

    private void addNotificationBookingFunctionality() {
        System.out.println("Adding notification booking functionality");
    }
}

public class ClientBookingSystem {
    public static void main(String[] args) {
        Booking basicBooking = new NotaryOffice();
        basicBooking.book();

        Booking decoratedBooking = new NotificationBookingDecorator(
                new ScheduleBookingDecorator(
                        new ClientBookingDecorator(new NotaryOffice())
                )
        );
        decoratedBooking.book();
    }
}
