# Context class representing the electronic client appointment system
class AppointmentSystem:
    def __init__(self, strategy):
        self.strategy = strategy

    def set_strategy(self, strategy):
        self.strategy = strategy

    def make_appointment(self, client):
        self.strategy.make_appointment(client)


# Abstract strategy class
class AppointmentStrategy:
    def make_appointment(self, client):
        pass


# Concrete strategy classes
class OnlineAppointmentStrategy(AppointmentStrategy):
    def make_appointment(self, client):
        print(f"Making online appointment for client '{client}'.")


class InPersonAppointmentStrategy(AppointmentStrategy):
    def make_appointment(self, client):
        print(f"Making in-person appointment for client '{client}'.")


# Usage example
if __name__ == '__main__':
    # Create the context instance
    appointment_system = AppointmentSystem(None)

    # Create different strategies
    online_strategy = OnlineAppointmentStrategy()
    in_person_strategy = InPersonAppointmentStrategy()

    # Set a strategy
    appointment_system.set_strategy(online_strategy)

    # Make an appointment using the selected strategy
    appointment_system.make_appointment("John Doe")

    # Change the strategy dynamically
    appointment_system.set_strategy(in_person_strategy)

    # Make another appointment using the new strategy
    appointment_system.make_appointment("Jane Smith")
