from abc import ABC, abstractmethod


# Receiver Class
class NotarySystem:
    def create_client_record(self, client_name):
        print(f"Creating client record for {client_name}...")

    def update_client_record(self, client_name):
        print(f"Updating client record for {client_name}...")

    def delete_client_record(self, client_name):
        print(f"Deleting client record for {client_name}...")


# Abstract Command Class
class Command(ABC):
    @abstractmethod
    def execute(self):
        pass


# Concrete Command Classes
class CreateClientRecordCommand(Command):
    def __init__(self, notary_system, client_name):
        self.notary_system = notary_system
        self.client_name = client_name

    def execute(self):
        self.notary_system.create_client_record(self.client_name)


class UpdateClientRecordCommand(Command):
    def __init__(self, notary_system, client_name):
        self.notary_system = notary_system
        self.client_name = client_name

    def execute(self):
        self.notary_system.update_client_record(self.client_name)


class DeleteClientRecordCommand(Command):
    def __init__(self, notary_system, client_name):
        self.notary_system = notary_system
        self.client_name = client_name

    def execute(self):
        self.notary_system.delete_client_record(self.client_name)


# Invoker Class
class NotaryOfficeAssistant:
    def __init__(self):
        self.command_queue = []

    def add_command(self, command):
        self.command_queue.append(command)

    def execute_commands(self):
        for command in self.command_queue:
            command.execute()
        self.command_queue.clear()


# Client
if __name__ == '__main__':
    notary_system = NotarySystem()
    assistant = NotaryOfficeAssistant()

    create_record_command = CreateClientRecordCommand(notary_system, "John Doe")
    update_record_command = UpdateClientRecordCommand(notary_system, "John Doe")
    delete_record_command = DeleteClientRecordCommand(notary_system, "John Doe")

    assistant.add_command(create_record_command)
    assistant.add_command(update_record_command)
    assistant.add_command(delete_record_command)

    assistant.execute_commands()
