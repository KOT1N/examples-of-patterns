class ClientRecordSystem:
    instance = None
    
    @staticmethod
    def get_instance():
        if not ClientRecordSystem.instance:
            ClientRecordSystem.instance = ClientRecordSystem()
        return ClientRecordSystem.instance
    
    def __init__(self):
        self.client_records = []
    
    def add_client_record(self, client_record):
        self.client_records.append(client_record)
    
    def remove_client_record(self, client_record):
        self.client_records.remove(client_record)
    
    def display_client_records(self):
        for record in self.client_records:
            print(record)
    
class ClientRecord:
    def __init__(self, name, file_number):
        self.name = name
        self.file_number = file_number
    
    def __str__(self):
        return f"Client: {self.name}, File Number: {self.file_number}"


# Usage
record_system = ClientRecordSystem.get_instance()

# Add client records to the system
record_system.add_client_record(ClientRecord("John Doe", "ABC123"))
record_system.add_client_record(ClientRecord("Jane Smith", "XYZ789"))

# Display all client records
record_system.display_client_records()

# Output:
# Client: John Doe, File Number: ABC123
# Client: Jane Smith, File Number: XYZ789
