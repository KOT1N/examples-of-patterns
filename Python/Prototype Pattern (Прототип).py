import copy

class NotaryClientAppointment:
    def __init__(self, client_name, appointment_date, appointment_time):
        self.client_name = client_name
        self.appointment_date = appointment_date
        self.appointment_time = appointment_time

    def clone(self):
        return copy.deepcopy(self)

    def __str__(self):
        return f"Client Name: {self.client_name}, Appointment Date: {self.appointment_date}, Appointment Time: {self.appointment_time}"


class NotaryClientAppointmentPrototype:
    def __init__(self):
        self.appointment_prototype = {}

    def register(self, appointment_type, appointment):
        self.appointment_prototype[appointment_type] = appointment

    def unregister(self, appointment_type):
        del self.appointment_prototype[appointment_type]

    def clone(self, appointment_type, **kwargs):
        appointment = self.appointment_prototype.get(appointment_type)
        if appointment:
            new_appointment = appointment.clone()
            new_appointment.client_name = kwargs.get('client_name', new_appointment.client_name)
            new_appointment.appointment_date = kwargs.get('appointment_date', new_appointment.appointment_date)
            new_appointment.appointment_time = kwargs.get('appointment_time', new_appointment.appointment_time)
            return new_appointment
        return None

if __name__ == '__main__':
    appointment_prototype = NotaryClientAppointmentPrototype()

    # Registering different types of appointments
    appointment = NotaryClientAppointment("John Doe", "2022-12-31", "10:00 AM")
    appointment_prototype.register("appointment", appointment)

    # Cloning existing appointment
    cloned_appointment = appointment_prototype.clone("appointment")
    if cloned_appointment:
        print(cloned_appointment)

    # Cloning appointment with updated details
    updated_appointment = appointment_prototype.clone("appointment", client_name="Jane Smith", appointment_time="2:00 PM")
    if updated_appointment:
        print(updated_appointment)
