# Subsystem classes
class Client:
    def __init__(self, name):
        self.name = name
    
    def __str__(self):
        return self.name

class Schedule:
    def __init__(self):
        self.appointments = []
    
    def add_appointment(self, appointment):
        self.appointments.append(appointment)
    
    def remove_appointment(self, appointment):
        self.appointments.remove(appointment)
    
    def show_schedule(self):
        print("Appointments:")
        for appointment in self.appointments:
            print(appointment)

class Notary:
    def __init__(self, name):
        self.name = name
    
    def __str__(self):
        return self.name

class Notification:
    def send_notification(self, client):
        print(f"Notification sent to {client}.")

# Facade class
class ElectronicAppointmentSystem:
    def __init__(self, notary_name):
        self.client = Client("")
        self.schedule = Schedule()
        self.notary = Notary(notary_name)
        self.notification = Notification()
    
    def book_appointment(self, client_name):
        self.client = Client(client_name)
        appointment = f"Appointment booked with {self.notary}"
        self.schedule.add_appointment(appointment)
        self.notification.send_notification(self.client)
    
    def cancel_appointment(self):
        appointment = f"Appointment booked with {self.notary}"
        self.schedule.remove_appointment(appointment)
    
    def show_appointments(self):
        self.schedule.show_schedule()

# Client code
def main():
    system = ElectronicAppointmentSystem("John Doe")

    system.book_appointment("Alice")
    system.book_appointment("Bob")
    system.book_appointment("Carol")

    system.show_appointments()

    system.cancel_appointment()

    system.show_appointments()

if __name__ == "__main__":
    main()
