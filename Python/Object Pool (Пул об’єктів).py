class Client:
    def __init__(self, name):
        self.name = name

class ClientRecordPool:
    def __init__(self, max_clients):
        self.max_clients = max_clients
        self.client_pool = []
        self.client_records = []

        # Initialize the client pool with empty client objects
        for _ in range(max_clients):
            self.client_pool.append(Client(None))

    def get_client_record(self, name):
        if self.client_records:
            # Get a client record from the pool if available
            client_record = self.client_records.pop()
            client_record.name = name
        else:
            # Create a new client record since the pool is empty
            client_record = Client(name)

        return client_record

    def release_client_record(self, client_record):
        # Check if the pool is already full
        if len(self.client_records) < self.m