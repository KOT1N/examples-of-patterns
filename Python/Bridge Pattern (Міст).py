# Implementing the Bridge Pattern for Electronic Client Booking System in a Notary Office

# Abstract class for the Client
class Client:
    def __init__(self, booking_system):
        self.booking_system = booking_system

    def book(self):
        pass

# Concrete class for Individual Clients
class IndividualClient(Client):
    def book(self):
        print("Booking an appointment for an individual client")
        self.booking_system.book_appointment()

# Concrete class for Corporate Clients
class CorporateClient(Client):
    def book(self):
        print("Booking an appointment for a corporate client")
        self.booking_system.book_appointment()

# Abstract class for the Booking System
class BookingSystem:
    def book_appointment(self):
        pass

# Concrete implementation of the Booking System for Online Booking
class OnlineBookingSystem(BookingSystem):
    def book_appointment(self):
        print("Booking the appointment online")

# Concrete implementation of the Booking System for Phone Booking
class PhoneBookingSystem(BookingSystem):
    def book_appointment(self):
        print("Booking the appointment over the phone")

# Main program
if __name__ == '__main__':
    # Creating an instance of the Online Booking System
    online_booking_system = OnlineBookingSystem()

    # Creating an instance of the Phone Booking System
    phone_booking_system = PhoneBookingSystem()

    # Creating an instance of the Individual Client and booking an appointment using the Online Booking System
    individual_client = IndividualClient(online_booking_system)
    individual_client.book()

    # Creating an instance of the Corporate Client and booking an appointment using the Phone Booking System
    corporate_client = CorporateClient(phone_booking_system)
    corporate_client.book()
