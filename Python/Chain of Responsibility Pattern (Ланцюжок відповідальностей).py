class AppointmentRequest:
    def __init__(self, name, date, time):
        self.name = name
        self.date = date
        self.time = time


class Handler:
    def __init__(self, successor=None):
        self.successor = successor

    def handle_request(self, appointment):
        pass


class NotaryHandler(Handler):
    def handle_request(self, appointment):
        if appointment.date == '2022-01-01':
            print(f"Appointment for {appointment.name} on {appointment.date} at {appointment.time} is scheduled with the notary.")
        elif self.successor is not None:
            self.successor.handle_request(appointment)


class AssistantHandler(Handler):
    def handle_request(self, appointment):
        if appointment.date == '2022-01-02':
            print(f"Appointment for {appointment.name} on {appointment.date} at {appointment.time} is scheduled with the assistant.")
        elif self.successor is not None:
            self.successor.handle_request(appointment)


class ReceptionistHandler(Handler):
    def handle_request(self, appointment):
        if appointment.date == '2022-01-03':
            print(f"Appointment for {appointment.name} on {appointment.date} at {appointment.time} is scheduled with the receptionist.")
        elif self.successor is not None:
            self.successor.handle_request(appointment)


if __name__ == '__main__':
    # Creating the chain of responsibility
    receptionist_handler = ReceptionistHandler()
    assistant_handler = AssistantHandler(receptionist_handler)
    notary_handler = NotaryHandler(assistant_handler)

    # Simulating appointment requests
    appointment1 = AppointmentRequest("John", "2022-01-01", "10:00")
    notary_handler.handle_request(appointment1)

    appointment2 = AppointmentRequest("Jane", "2022-01-02", "14:30")
    notary_handler.handle_request(appointment2)

    appointment3 = AppointmentRequest("Joe", "2022-01-03", "09:00")
    notary_handler.handle_request(appointment3)
