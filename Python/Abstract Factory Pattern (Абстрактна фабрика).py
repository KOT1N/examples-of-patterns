from abc import ABC, abstractmethod

# Abstract Product: Client
class Client(ABC):
    @abstractmethod
    def display_client_details(self):
        pass

# Concrete Product A: RegularClient
class RegularClient(Client):
    def display_client_details(self):
        print("Regular Client")

# Concrete Product B: VIPClient
class VIPClient(Client):
    def display_client_details(self):
        print("VIP Client")

# Abstract Factory: AbstractAppointmentsFactory
class AbstractAppointmentsFactory(ABC):
    @abstractmethod
    def create_client(self) -> Client:
        pass

# Concrete Factory A: RegularAppointmentsFactory
class RegularAppointmentsFactory(AbstractAppointmentsFactory):
    def create_client(self) -> Client:
        return RegularClient()

# Concrete Factory B: VIPAppointmentsFactory
class VIPAppointmentsFactory(AbstractAppointmentsFactory):
    def create_client(self) -> Client:
        return VIPClient()

# Client code
def main():
    regular_factory = RegularAppointmentsFactory()
    regular_client = regular_factory.create_client()
    regular_client.display_client_details()

    vip_factory = VIPAppointmentsFactory()
    vip_client = vip_factory.create_client()
    vip_client.display_client_details()

if __name__ == '__main__':
    main()
