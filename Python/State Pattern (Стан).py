from abc import ABC, abstractmethod


class AppointmentState(ABC):
    @abstractmethod
    def book_appointment(self):
        pass

    @abstractmethod
    def reschedule_appointment(self):
        pass

    @abstractmethod
    def cancel_appointment(self):
        pass


class AvailableState(AppointmentState):
    def book_appointment(self):
        print("Appointment booked successfully.")
        return BookedState()

    def reschedule_appointment(self):
        print("No appointment to reschedule.")

    def cancel_appointment(self):
        print("No appointment to cancel.")


class BookedState(AppointmentState):
    def book_appointment(self):
        print("Appointment is already booked.")

    def reschedule_appointment(self):
        print("Appointment rescheduled successfully.")
        return BookedState()

    def cancel_appointment(self):
        print("Appointment canceled successfully.")
        return AvailableState()


class Appointment:
    def __init__(self):
        self.state = AvailableState()

    def book(self):
        self.state = self.state.book_appointment()

    def reschedule(self):
        self.state = self.state.reschedule_appointment()

    def cancel(self):
        self.state = self.state.cancel_appointment()


# Usage example:
if __name__ == '__main__':
    appointment = Appointment()
    
    # Available state - book appointment
    appointment.book()
    
    # Booked state - try to book again
    appointment.book()
    
    # Booked state - reschedule appointment
    appointment.reschedule()
    
    # Available state - cancel appointment
    appointment.cancel()
    
    # Available state - try to cancel again
    appointment.cancel()
