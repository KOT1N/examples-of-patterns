# Interface for the base NotaryService
class NotaryService:
    def schedule_appointment(self, date):
        pass
    
# Concrete implementation of NotaryService
class BasicNotaryService(NotaryService):
    def schedule_appointment(self, date):
        print(f"Basic Notary Service: Scheduled an appointment for {date}")
        
# Abstract decorator class that extends NotaryService
class NotaryServiceDecorator(NotaryService):
    def __init__(self, notary_service):
        self.notary_service = notary_service
        
    def schedule_appointment(self, date):
        self.notary_service.schedule_appointment(date)
        
# Concrete decorator class that adds additional feature
class PriorityNotaryService(NotaryServiceDecorator):
    def __init__(self, notary_service):
        super().__init__(notary_service)
        
    def schedule_appointment(self, date):
        super().schedule_appointment(date)
        print("Priority Notary Service: Appointment confirmation email sent.")
        
# Create a basic notary service object
basic_service = BasicNotaryService()

# Create a priority notary service decorator with basic notary service as its base
priority_service = PriorityNotaryService(basic_service)

# Schedule an appointment using the priority notary service
priority_service.schedule_appointment("2022-01-01")
