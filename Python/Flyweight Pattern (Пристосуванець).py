from typing import Dict


class NotaryOffice:
    def __init__(self):
        self.notaries: Dict[str, Notary] = {}

    def get_notary(self, name: str) -> Notary:
        if name not in self.notaries:
            self.notaries[name] = Notary(name)
        return self.notaries[name]

    def make_appointment(self, notary_name: str, customer_name: str, date: str, time: str):
        notary = self.get_notary(notary_name)
        notary.make_appointment(customer_name, date, time)

    def show_schedule(self):
        for notary_name, notary in self.notaries.items():
            print(f"Notary: {notary_name}")
            notary.show_appointments()


class Notary:
    def __init__(self, name: str):
        self.name = name
        self.appointments = {}

    def make_appointment(self, customer_name: str, date: str, time: str):
        appointment = Appointment(customer_name, date, time)
        self.appointments[appointment.get_id()] = appointment

    def show_appointments(self):
        for appointment_id, appointment in self.appointments.items():
            print(f"Appointment {appointment_id}: {appointment}")

class Appointment:
    id_counter = 1

    def __init__(self, customer_name: str, date: str, time: str):
        self.id = Appointment.id_counter
        Appointment.id_counter += 1
        self.customer_name = customer_name
        self.date = date
        self.time = time

    def get_id(self) -> int:
        return self.id

    def __str__(self):
        return f"Customer: {self.customer_name}, Date: {self.date}, Time: {self.time}"


# Usage Example
notary_office = NotaryOffice()

# Make appointments for Notary 1
notary_office.make_appointment("Notary 1", "John Doe", "2022-01-01", "10:00")
notary_office.make_appointment("Notary 1", "Jane Smith", "2022-01-02", "14:30")

# Make appointments for Notary 2
notary_office.make_appointment("Notary 2", "Alice Williams", "2022-01-03", "09:00")
notary_office.make_appointment("Notary 2", "Bob Johnson", "2022-01-04", "16:00")

# Show the schedule for all notaries
notary_office.show_schedule()
