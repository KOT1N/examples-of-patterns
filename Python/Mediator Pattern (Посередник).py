from datetime import datetime

class NotaryOffice:
    def __init__(self):
        self.mediator = AppointmentMediator()

    def book_appointment(self, client, date):
        self.mediator.book_appointment(client, date)

    def cancel_appointment(self, client, date):
        self.mediator.cancel_appointment(client, date)

    def show_appointments(self):
        self.mediator.show_appointments()


class AppointmentMediator:
    def __init__(self):
        self.appointments = {}

    def book_appointment(self, client, date):
        if date not in self.appointments:
            self.appointments[date] = client
            print(f"Appointment booked for {client} on {date}.")
        else:
            print(f"Appointment already booked for {self.appointments[date]} on {date}. "
                  "Please choose another date.")

    def cancel_appointment(self, client, date):
        if date in self.appointments and self.appointments[date] == client:
            del self.appointments[date]
            print(f"Appointment cancelled for {client} on {date}.")
        else:
            print(f"No appointment found for {client} on {date}."
                  " Please provide correct details.")

    def show_appointments(self):
        if len(self.appointments) == 0:
            print("No appointments scheduled.")
        else:
            print("Scheduled appointments:")
            for date, client in self.appointments.items():
                print(f"{client} - {date}")


# Example usage
office = NotaryOffice()

office.show_appointments()  # No appointments scheduled

office.book_appointment("John", datetime(2022, 9, 15, 9, 0))
office.book_appointment("Alice", datetime(2022, 9, 15, 14, 30))
office.book_appointment("Bob", datetime(2022, 9, 16, 10, 0))

office.show_appointments()
# Scheduled appointments:
# John - 2022-09-15 09:00:00
# Alice - 2022-09-15 14:30:00
# Bob - 2022-09-16 10:00:00

office.cancel_appointment("Alice", datetime(2022, 9, 15, 14, 30))
# Appointment cancelled for Alice on 2022-09-15 14:30:00

office.show_appointments()
# Scheduled appointments:
# John - 2022-09-15 09:00:00
# Bob - 2022-09-16 10:00:00

office.book_appointment("John", datetime(2022, 9, 15, 9, 0))
# Appointment already booked for John on 2022-09-15 09:00:00.
# Please choose another date.

