from abc import ABC, abstractmethod

# Visitor interface
class Visitor(ABC):
    @abstractmethod
    def visit(self, element):
        pass

# Element interface
class Element(ABC):
    @abstractmethod
    def accept(self, visitor):
        pass

# Concrete element: Customer
class Customer(Element):
    def __init__(self, name):
        self.name = name

    def accept(self, visitor):
        visitor.visit(self)

# Concrete visitor: AppointmentNotifier
class AppointmentNotifier(Visitor):
    def visit(self, element):
        if isinstance(element, Customer):
            print(f"Notifying customer {element.name} about their appointment via email.")

# Application code
if __name__ == "__main__":
    # Create customers
    customer1 = Customer("John")
    customer2 = Customer("Jane")

    # Create visitor
    notifier = AppointmentNotifier()

    # Accept visitor on elements
    customer1.accept(notifier)
    customer2.accept(notifier)
