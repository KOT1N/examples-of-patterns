# Existing NotaryOffice class
class NotaryOffice:
    def book_appointment(self, customer):
        print("Booking an appointment for customer:", customer)


# Existing Customer class
class Customer:
    def __init__(self, name):
        self.name = name


# Adapter class
class BookingAdapter:
    def __init__(self, customer):
        self.customer = customer

    def book(self):
        notary_office = NotaryOffice()
        notary_office.book_appointment(self.customer.name)


# Usage
if __name__ == "__main__":
    # Existing customer object
    customer = Customer("John Doe")

    # Adapter usage
    adapter = BookingAdapter(customer)
    adapter.book()
