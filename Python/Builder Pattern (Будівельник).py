class Customer:
    def __init__(self):
        self.name = ""
        self.appointment_date = ""
        self.appointment_time = ""
        self.document_type = ""
        self.is_confirmed = False

    def __str__(self):
        confirmation_status = "Confirmed" if self.is_confirmed else "Not confirmed"
        return f"Name: {self.name}\nAppointment Date: {self.appointment_date}\nAppointment Time: {self.appointment_time}\nDocument Type: {self.document_type}\nConfirmation: {confirmation_status}"


class AppointmentBuilder:
    def __init__(self):
        self.customer = Customer()

    def set_name(self, name):
        self.customer.name = name

    def set_appointment_date(self, date):
        self.customer.appointment_date = date

    def set_appointment_time(self, time):
        self.customer.appointment_time = time

    def set_document_type(self, doc_type):
        self.customer.document_type = doc_type

    def confirm_appointment(self):
        self.customer.is_confirmed = True

    def get_appointment(self):
        return self.customer


# Usage Example
builder = AppointmentBuilder()
builder.set_name("John Doe")
builder.set_appointment_date("2021-09-01")
builder.set_appointment_time("10:00 AM")
builder.set_document_type("Power of Attorney")
builder.confirm_appointment()

appointment = builder.get_appointment()
print(appointment)
