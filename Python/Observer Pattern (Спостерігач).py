from abc import ABC, abstractmethod

class Observer(ABC):
    @abstractmethod
    def update(self):
        pass

class Subject(ABC):
    def __init__(self):
        self.observers = []

    def attach(self, observer):
        self.observers.append(observer)

    def detach(self, observer):
        self.observers.remove(observer)

    def notify(self):
        for observer in self.observers:
            observer.update()

class NotaryOffice(Subject):
    def __init__(self):
        super().__init__()
        self.appointments = []
        self.available_slots = []

    def set_available_slots(self, slots):
        self.available_slots = slots
        self.notify()

    def add_appointment(self, appointment):
        self.appointments.append(appointment)
        self.notify()

class Customer:
    def __init__(self, name):
        self.name = name

    def update(self):
        print(f"Customer '{self.name}' has been notified of the appointment availability.")

# Creating objects
office = NotaryOffice()
customer1 = Customer("John")
customer2 = Customer("Alice")

# Attaching customers to the office
office.attach(customer1)
office.attach(customer2)

# Updating the available slots
office.set_available_slots(["Monday 10 AM", "Wednesday 2 PM", "Friday 4 PM"])

# Adding an appointment
office.add_appointment("Monday 10 AM")

# Detaching a customer
office.detach(customer2)

# Adding another appointment
office.add_appointment("Monday 10 AM")
