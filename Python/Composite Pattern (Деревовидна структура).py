# Base class for all components in the tree structure
class ClientRecordComponent:
    def __init__(self, name):
        self.name = name

    def add(self, component):
        pass

    def remove(self, component):
        pass

    def display(self, level=0):
        pass


# Leaf class representing single client record
class ClientRecord(ClientRecordComponent):
    def add(self, component):
        print("Cannot add to a client record.")

    def remove(self, component):
        print("Cannot remove from a client record.")

    def display(self, level=0):
        prefix = "    " * level
        print(f"{prefix}- {self.name}")


# Composite class representing a folder containing multiple client records
class ClientRecordFolder(ClientRecordComponent):
    def __init__(self, name):
        super().__init__(name)
        self.records = []

    def add(self, component):
        self.records.append(component)

    def remove(self, component):
        self.records.remove(component)

    def display(self, level=0):
        prefix = "    " * level
        print(f"{prefix}+ {self.name} (Folder)")

        for record in self.records:
            record.display(level + 1)


# Usage example
if __name__ == "__main__":
    # Create client records
    client1 = ClientRecord("John Doe")
    client2 = ClientRecord("Jane Smith")
    client3 = ClientRecord("Alice Johnson")

    # Create folders
    folder1 = ClientRecordFolder("Clients")
    folder2 = ClientRecordFolder("Archived Clients")

    # Add client records to the folders
    folder1.add(client1)
    folder1.add(client2)
    folder2.add(client3)

    # Add folder2 to folder1
    folder1.add(folder2)

    # Display the tree structure
    folder1.display()
