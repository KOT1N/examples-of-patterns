class Client:
    def __init__(self, name, appointment_date):
        self.name = name
        self.appointment_date = appointment_date

class ClientRecord:
    def __init__(self):
        self.clients = []

    def add_client(self, client):
        self.clients.append(client)

    def remove_client(self, client):
        self.clients.remove(client)

    def __iter__(self):
        return ClientIterator(self.clients)

class ClientIterator:
    def __init__(self, clients):
        self.clients = clients
        self.current = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.current < len(self.clients):
            client = self.clients[self.current]
            self.current += 1
            return client
        else:
            raise StopIteration

# Usage example
record = ClientRecord()

# Add clients to the record
record.add_client(Client("John Smith", "2021-03-10"))
record.add_client(Client("Alice Johnson", "2021-03-15"))
record.add_client(Client("Robert Davis", "2021-03-20"))

# Iterate over the clients using the iterator pattern
iterator = iter(record)
while True:
    try:
        client = next(iterator)
        print(f"Client Name: {client.name}, Appointment Date: {client.appointment_date}")
    except StopIteration:
        break
