from abc import ABC, abstractmethod

class CustomerRecordSystem(ABC):

    def record_customer(self):
        self.verify_customer_info()
        self.save_customer_info()
        self.send_confirmation()

    @abstractmethod
    def verify_customer_info(self):
        pass

    @abstractmethod
    def save_customer_info(self):
        pass

    def send_confirmation(self):
        print("Customer information saved successfully.")
        print("Confirmation email sent to the customer.")

class NotaryOfficeCRM(CustomerRecordSystem):

    def verify_customer_info(self):
        print("Verifying customer information...")
        print("Customer information has been verified.")

    def save_customer_info(self):
        print("Saving customer information to the database...")

        # Code to save customer information to the database goes here

        print("Customer information has been saved.")

notary_crm = NotaryOfficeCRM()
notary_crm.record_customer()
