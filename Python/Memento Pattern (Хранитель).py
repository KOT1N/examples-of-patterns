class Memento:
    def __init__(self, state):
        self._state = state

    def get_state(self):
        return self._state


class Caretaker:
    def __init__(self):
        self._mementos = []

    def add_memento(self, memento):
        self._mementos.append(memento)

    def get_memento(self, index):
        return self._mementos[index]


class Customer:
    def __init__(self, name, email):
        self._name = name
        self._email = email

    def __str__(self):
        return f"Name: {self._name}, Email: {self._email}"

    def save_memento(self):
        return Memento(self.__dict__.copy())

    def restore_from_memento(self, memento):
        self.__dict__ = memento.get_state()


class CustomerRecordSystem:
    def __init__(self):
        self._customers = []
        self._caretaker = Caretaker()

    def add_customer(self, customer):
        self._customers.append(customer)
        self._caretaker.add_memento(customer.save_memento())

    def undo_last_customer(self):
        if len(self._caretaker._mementos) > 1:
            self._caretaker._mementos.pop()
            last_memento_state = self._caretaker.get_memento(-1).get_state()
            self._customers[-1].restore_from_memento(last_memento_state)
        else:
            print("Cannot undo customer creation.")

    def show_all_customers(self):
        for customer in self._customers:
            print(customer)


# Example usage
record_system = CustomerRecordSystem()

customer1 = Customer("John Doe", "john@example.com")
record_system.add_customer(customer1)

customer2 = Customer("Jane Smith", "jane@example.com")
record_system.add_customer(customer2)

# Before undo
print("Customers:")
record_system.show_all_customers()
print()

# Undo the last customer creation
record_system.undo_last_customer()

# After undo
print("Customers after undo:")
record_system.show_all_customers()
