from abc import ABC, abstractmethod

# Abstract class representing the Customer
class Customer(ABC):
    @abstractmethod
    def get_appointment_confirmation(self):
        pass

# Concrete implementation of Customer representing a notary client
class NotaryCustomer(Customer):
    def get_appointment_confirmation(self):
        return "Appointment confirmed for a notary customer."


# Abstract class representing the Appointment Creator
class AppointmentCreator(ABC):
    @abstractmethod
    def create_customer(self) -> Customer:
        pass

    def confirm_appointment(self) -> str:
        customer = self.create_customer()
        confirmation = customer.get_appointment_confirmation()
        return confirmation


# Concrete implementation of AppointmentCreator for notary customers
class NotaryAppointmentCreator(AppointmentCreator):
    def create_customer(self) -> Customer:
        return NotaryCustomer()


# Client code
def main():
    # Creating an instance of NotaryAppointmentCreator
    appointment_creator = NotaryAppointmentCreator()
  
    # Confirmation for notary customer appointment
    confirmation = appointment_creator.confirm_appointment()
    print(confirmation)

if __name__ == '__main__':
    main()
